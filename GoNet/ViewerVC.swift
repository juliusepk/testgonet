//
//  ViewerVC.swift
//  GoNet
//
//  Created by Julio Serrano on 15/07/20.
//  Copyright © 2020 Julio Serrano. All rights reserved.
//

import UIKit

class ViewerVC: UIViewController {
    @IBOutlet weak var lblTitleViewer: UILabel!
    @IBOutlet weak var mainImageViewer: UIImageView!
    @IBOutlet weak var lblDescription: UILabel!
    
    var currentItem: modelItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblTitleViewer.text = currentItem.title
        lblDescription.text = currentItem.descript
        mainImageViewer.image = currentItem.mainImage
    }
    
}
