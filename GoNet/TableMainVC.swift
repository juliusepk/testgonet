//
//  TableMainVC.swift
//  GoNet
//
//  Created by Julio Serrano on 15/07/20.
//  Copyright © 2020 Julio Serrano. All rights reserved.
//

import UIKit

struct modelItem {
    let mainImage: UIImage?
    let title: String?
    let descript: String?
}

class TableMainVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var cvContent: UICollectionView!
    var data = [modelItem]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        data = [
            modelItem(mainImage: UIImage(named: "callOfDuty"), title: "Title 1", descript: "Description 1"),
            modelItem(mainImage: UIImage(named: "callOfDuty"), title: "Title 2", descript: "Description 2"),
            modelItem(mainImage: UIImage(named: "callOfDuty"), title: "Title 3", descript: "Description 3"),
            modelItem(mainImage: UIImage(named: "callOfDuty"), title: "Title 4", descript: "Description 4")
        ]
    }
    
    // mark: prepare segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueShowDetail" {
            if let viewerItem = segue.destination as? ViewerVC {
                if let item = sender as? modelItem {
                    viewerItem.currentItem = item
                }
            }
        }
    }
    
    
    // mark: UICollectionView
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = cvContent.dequeueReusableCell(withReuseIdentifier: "idCell", for: indexPath) as! CustomCell
        cell.mainImage.image = data[indexPath.row].mainImage
        cell.lblTitle.text = data[indexPath.row].title
        cell.lblDescription.text = data[indexPath.row].descript
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = cvContent.bounds.size
        let width = size.width
        return CGSize(width: width, height: 350.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "segueShowDetail", sender: data[indexPath.row])
    }

}
