//
//  CustomCell.swift
//  GoNet
//
//  Created by Julio Serrano on 15/07/20.
//  Copyright © 2020 Julio Serrano. All rights reserved.
//

import UIKit

class CustomCell: UICollectionViewCell {
    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
}


