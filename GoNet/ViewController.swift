//
//  ViewController.swift
//  GoNet
//
//  Created by Julio Serrano on 15/07/20.
//  Copyright © 2020 Julio Serrano. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var containerThreadsVC: UIView!
    @IBOutlet weak var containerTableVC: UIView!
    
    var stackViews: [UIView]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        containerThreadsVC.isHidden = true
        containerTableVC.isHidden = false
    }
    
    @IBAction func actionSegmented(_ sender: UISegmentedControl) {
        
        switch sender.selectedSegmentIndex {
        case 0:
            containerThreadsVC.isHidden = true
            containerTableVC.isHidden = false
        case 1:
            containerThreadsVC.isHidden = false
            containerTableVC.isHidden = true
        default:
            break
        }
    }



}

